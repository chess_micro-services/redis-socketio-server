const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const { spawn } = require('child_process');
spawn('python3', ['pawn.py']);

const { pub, hmsetAsync, sub, piece, noGame } = require("./data.js");

let userCount = 0;// total number of players in all of the games

pub.get('defaultStartState', (err,value) => {
    pub.hmset('Pre-game', 'boardState', value);// set pre-game board state
});

let setBoardState = (room) => {
	console.log('set board', room);
	io.sockets.in(room).emit('clean-square', 'all');
	pub.hmget(room, 'boardState', (err, value) =>{
		let boardState = JSON.parse(value);
		// console.log('setBoardState', room, boardState);
		for ( let player in boardState ){
			// console.log(player)
			for ( let pieceType in boardState[player] ){
				// console.log(boardState[player][pieceType]);
				for ( let id in boardState[player][pieceType] ){
					// console.log( player, pieceType, piece[pieceType]);
					io.sockets.in(room).emit('set-piece-on-square', {
						"id": boardState[player][pieceType][id],
						"player": player,
						"type": pieceType,
						"display": piece[player][pieceType]
					});
				}
			}
		}
	});
};

function joinGameInGames(user){
	pub.hgetall(user.gameCode, (err,game) => {
		// console.log('join-game-in-games', user.gameCode, game);
		if( game != null ){
			if(user.player == 'white'){
				if (game.black == user.name){
					pub.hmset(user.gameCode, 
						'white', user.name,
						'black', ''
					);
				} else {
					pub.hmset(user.gameCode, "white", user.name);
				}
			} else if(user.player == 'black') {
				if (game.white == user.name){
					pub.hmset(user.gameCode, 
						'black', user.name,
						'white', ''
					);
				} else {
					pub.hmset(user.gameCode, "black", user.name);
				}
			}
			pub.hmset(user.gameCode, 'users', ++game.users );
		}
	});
}

io.on('connection', (socket) => {
	userCount++;
	console.log('user connected ' + userCount + ' user(s)');
	
	socket.emit('get-user');
	socket.on('send-user', user => {
		pub.hgetall(user.gameCode, (err, game)=>{
			console.log("send-user", user, game);
			if( game != null ){
				joinGameInGames(user);
				socket.join(user.gameCode);
				io.sockets.in(user.gameCode).emit('update-settings', {
					black: game.black,
					white: game.white,
					turn: game.turn,
					code: user.gameCode,
					inGame: true
				});
				setBoardState(user.gameCode);
			} else {
				socket.emit('update-settings', noGame);
				socket.emit('clean-square', 'all');
			}
		});
		
	});
	
	socket.on('message', (message) => {
		console.log(message);
		io.sockets.emit('chat-message', message);
	});
	
	socket.on('disconnect', () => {
		userCount--;
		console.log('user disconnected ' + userCount + ' user(s)');
	});

	// adds a player to a room if game/room dose not exists creat then join.
	socket.on('create-join-game', (user) => {
		function joinRoom(err,game){
			console.log('joinRoom');
			if( game != null ){
				socket.join(user.gameCode);
				joinGameInGames(user);
				io.sockets.in(user.gameCode).emit('update-settings', {
					black: game.black,
					white: game.white,
					turn: game.turn,
					code: user.gameCode,
					inGame: true
				});

				io.sockets.in(user.gameCode).emit('chat-message', {
					name: 'System',
					message: user.name + ' joined game: ' + user.gameCode
				});
				setBoardState(user.gameCode);
				console.log('player is in game', user.gameCode);
			} else {
				console.log('create/join game FAILED');
				socket.emit('clean-square', 'all');
			}
		}
		console.log('create-join-game user:', user);
		pub.hgetall(user.gameCode, (err, game)=>{
			if( game == null ){
				console.log('new game ' + user.gameCode);
				pub.get('defaultStartState', async (err,value) => {
					await hmsetAsync(user.gameCode, 
						'white', '',
						'black', '',
						'turn', 'black',
						'boardState', value,
						'users', 0
					);
					// get the new game
					pub.hgetall(user.gameCode, joinRoom);
				});
			} else {
				console.log('game ' + user.gameCode + ' exists');
				joinRoom(null, game);
			}
		});
		
	});

	socket.on('leave-game', (user) => {
		console.log('leave-game', user);
		pub.hgetall(user.gameCode, (err, game)=>{
			if( game != null ){
				pub.hmset(user.gameCode, user.player, '');
				pub.hmset(user.gameCode, 'users', --game.users);
				
				// tell others in room you left
				socket.leave(user.gameCode);
				io.sockets.in(user.gameCode).emit('chat-message', {
					name: 'System',
					message: user.name + ' the ' + user.player + ' has left the game'
				});
				
				socket.emit('update-settings', noGame);
				socket.emit('clean-square', 'all');
			}
		});
		
		
	});

	socket.on('get-game', (gameCode) => {
		pub.hgetall(gameCode, (err,game)=>{
			if( game != null ){
				socket.emit('update-settings', {
					black: game.black,
					white: game.white,
					turn: game.turn,
					code: gameCode,
					inGame: true
				} );
				setBoardState(gameCode);
			} else {
				socket.emit('update-settings', noGame );
				socket.emit('clean-square', 'all');
			}
		});
		
	});

	socket.on('delete-game', gameCode => {
		pub.del(user.gameCode);
		socket.emit('update-settings', noGame);
	});

	socket.on('clean-board', (user) => {
		console.log('call clean board', user);
	  	io.sockets.in(user.gameCode).emit('clean-square', 'all');    
	});

	socket.on('set-board-to-state', (code) => {
		setBoardState(code);
	});

	socket.on('square-clicked', (item) => {
		console.log('square-clicked', item);
		pub.hmget(item.gameCode, 'turn', (err,turn)=>{
			if (item.highlight != null){
				socket.on('square-responce', (owner) => {
					// console.log('square-responce', item, 'new', owner);
					let movePiece = {
						code: item.code,
						newID: item.id,
						oldID: owner.id,
						type: 'move',
						player: owner.player
					};
					pub.publish(owner.type, JSON.stringify(movePiece));
				});
				let moveInfo = {
					id: item.highlightOwner,
				};
				io.sockets.in(item.code).emit('get-square', moveInfo);
			} else if (item.type != null){
				// console.log('item type to message ',  JSON.stringify(item));
				pub.publish(item.type, JSON.stringify(item));
			}
	
			io.sockets.in(item.code).emit('clean-square', 'highlight');
		});
		
	});

});

sub.on("message", function(channel, message) {
	let value = JSON.parse(message);
	
	console.log("Subscriber received message in channel '" + channel + "'\n\t", value);
	
	// consider a switch statment here
	if(channel == 'board'){
		// console.log('board');
		if (value.type == 'get'){
			console.log('get');
			io.sockets.in(value.gameCode).emit('get-square', value);
		}
		if (value.type == 'highlight'){
			console.log('highlight', value.gameCode, value);
			io.sockets.in(value.gameCode).emit('highlight-square', value);
		}
		if (value.type == 'move'){
			pub.hgetall(value.gameCode, async (err,game)=>{
				// console.log(game.boardState);
				let newState = game.boardState.replace(value.moveFrom, value.moveTo);
				pub.hmset(value.gameCode, 'boardState', newState);
				console.log(game.turn, (game.turn == 'white') ? 'black' : 'white');
				await pub.hmset(value.gameCode, 'turn', ((game.turn == 'white') ? 'black' : 'white' ));
			});
			io.sockets.in(value.gameCode).emit('log-move', {
				player: value.player,
				message: value.moveFrom + ' ' + value.type[0] + value.moveTo
			});
			
			// 
			// setBoardState(value.gameCode);
			io.sockets.in(value.gameCode).emit('clean-square', value.moveFrom,);
			io.sockets.in(value.gameCode).emit('set-piece-on-square', {
				id: value.moveTo,
				player: value.player,
				type: value.piece,
				display: piece[value.player][value.piece]
			});
		}
	}
});

sub.subscribe("board");

http.listen(9591, () => {
	console.log('socket server on port 9591');
});
