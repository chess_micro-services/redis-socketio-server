const redis = require("redis");
const { promisify } = require("util");

const publisher = redis.createClient(host="//192.168.1.3:6379");
const hmsetAsync = promisify(publisher.hmset).bind(publisher);

const subscriber = redis.createClient(host="//192.168.1.3:6379");

publisher.on("error", function(error) {
	console.error(error);
});
subscriber.on("error", function(error) {
	console.error(error);
});

subscriber.on("subscribe", function(channel, count) {
    publisher.publish("board", JSON.stringify( {"type": "test sucsesfull redis config"}));
    // publisher.publish("moves", JSON.stringify( {"key": "this is a move"}));
});

module.exports.pub = publisher;
module.exports.hmsetAsync = hmsetAsync;

module.exports.sub = subscriber;

// consider using https://msgpack.org/index.html
module.exports.piece = {
    white: {
        King: '\u2654',
        Queen: '\u2655',
        Rook: '\u2656',
        Bishop: '\u2657',
        Knight: '\u2658',
        Pawn: '\u2659'
    },
    black: {
        King: '\u265A',
        Queen: '\u265B',
        Rook: '\u265C',
        Bishop: '\u265D',
        Knight: '\u265E',
        Pawn: '\u265F'
    }
};

// publisher.hmset('piece-white', 
//     'King', '\u2654',
//     'Queen', '\u2655',
//     'Rook', '\u2656',
//     'Bishop', '\u2657',
//     'Knight', '\u2658',
//     'Pawn', '\u2659'
// );
// publisher.hmset('piece-black', 
//     'King', '\u265A',
//     'Queen', '\u265B',
//     'Rook', '\u265C',
//     'Bishop', '\u265D',
//     'Knight', '\u265E',
//     'Pawn', '\u265F' 
// );

publisher.set('defaultStartState', JSON.stringify({
	"black": { 
		"King": ['d1'],
		"Queen": ['e1'],
		"Rook": ['a1','h1'],
		"Bishop": ['c1','f1'],
		"Knight": ['b1','g1'],
		"Pawn": ['a2','b2','c2','d2','e2','f2','g2','h2'],
	},
	"white": {
		"King": ['d8'],
		"Queen": ['e8'],
		"Rook": ['a8','h8'],
		"Bishop": ['c8','f8'],
		"Knight": ['b8','g8'],
		"Pawn": ['a7','b7','c7','d7','e7','f7','g7','h7'],
	}
}));

module.exports.noGame = {
	black: '',
	white: '',
	turn: '',
	code: 'Pre-game',
	inGame: false
};

publisher.hmset('Pre-game', 
    'white', '',
    'black', '',
	'turn', 'black',
	'users', 0
);
