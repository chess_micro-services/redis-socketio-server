# redis-socketIO-server

## Install the dependencies...

```bash
git clone https://gitlab.com/chess_micro-services/redis-socketio-server socketIO
cd socketIO
npm install
```

set up your redis server
in redis.js line 12
<!-- brokers: ['localhost:9092'] -->

## run server...

```bash
npm start
```
